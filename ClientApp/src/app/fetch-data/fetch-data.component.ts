import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
    public forecasts: WeatherForecast[];
    checkoutForm;

    constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private formBuilder: FormBuilder,) {
      http.get<WeatherForecast[]>(baseUrl + 'weatherforecast').subscribe(result => {
        this.forecasts = result;
      }, error => console.error(error));

        this.checkoutForm = formBuilder.group({
            count: ''
        });
    }

    public remove(forecast: WeatherForecast) {
        this.http.delete<WeatherForecast>(this.baseUrl + 'weatherforecast/' + forecast.id).subscribe(result => {
            window.alert(`Succesfully deleted ${forecast.id}`);
            this.forecasts = this.forecasts.filter(x => x.id != forecast.id);
        }, error => console.error(error));
    }

    private onSubmit(formData) {
        this.http.post<WeatherForecast[]>(this.baseUrl + 'weatherforecast', formData.count).subscribe(result => {
            this.forecasts = result;
        }, error => console.error(error));
        this.checkoutForm.reset();
    }
}

interface WeatherForecast {
  id: number;
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}
