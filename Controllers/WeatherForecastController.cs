﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MyWebApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private static int Indexer = 0;

        private static List<WeatherForecast> weatherForecasts = Enumerable.Range(1, 5).Select(index => new WeatherForecast
        {
            Id = Indexer++,
            Date = DateTime.Now.AddDays(index),
            TemperatureC = new Random().Next(-20, 55),
            Summary = Summaries[new Random().Next(Summaries.Length)]
        }).ToList();

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            return weatherForecasts;
        }

        [HttpPost]
        public ActionResult<IEnumerable<WeatherForecast>> GenerateItems([FromBody]int count)
        {
            if (count < 1)
                return BadRequest();

            weatherForecasts = Enumerable.Range(1, count).Select(index => new WeatherForecast
            {
                Id = Indexer++,
                Date = DateTime.Now.AddDays(index),
                TemperatureC = new Random().Next(-20, 55),
                Summary = Summaries[new Random().Next(Summaries.Length)]
            }).ToList();

            return weatherForecasts;
        }

        [HttpDelete("{id}")]
        public ActionResult<WeatherForecast> Delete(int id)
        {
            var toDelete = weatherForecasts.FirstOrDefault(x => x.Id == id);
            if (toDelete == null)
                return NotFound();

            weatherForecasts.Remove(toDelete);
            return toDelete;
        }
    }
}
